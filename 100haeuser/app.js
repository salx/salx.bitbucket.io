/*
ToDO:
LADE-issue mobil 
Audio-player mobile...
Landscape Modus

*/

var map;

var w = document.documentElement.clientWidth
var zoomLevel = 7

if( w < 600 ) {
  zoomLevel = 6
}

//console.log(parent.document.getElementById("googleMap"));
//console.log(document.getElementById("googleMap"))

document.getElementById("googleMap").style.width="100%";
document.getElementById("googleMap").style.height="435px";

/*
if( w > 770){
  document.getElementById("googleMap").style.width="778px";
  document.getElementById("googleMap").style.height="435px";
}else if( w == 582 ){
  document.getElementById("googleMap").style.width="582px";
  document.getElementById("googleMap").style.height="288px";
}
*/

//var name = parent.document.getElementsByTagName("iframe")[0].className;

var centerLat;
var centerLong;

//console.log(centerLat,centerLong)
//hier die class des iFrames hineinschreiben

function initialize(){

  var zoomLevelButton = document.getElementById( 'zoomButton' )

  //define styles documentation: https://developers.google.com/maps/documentation/javascript/3.exp/reference#MapTypeStyle
  var klangOEStyles = [
      /*
      {
        featureType: "all",
        stylers:[
          { color: "#eef2f6"},
        ]
      },*/{
        featureType: "all",
        elementType: "labels",
        stylers:[
          {visibility: "off"}
        ]
      },{
        featureType:"water",
        elementType:"geometry",
          stylers:[
          { visibility: "off" }
        ]
      },{
        featureType:"administrative.locality",
        stylers:[
          { color: "#b70e0b" }
        ]
      },{
        featureType: "road",
        stylers:[
          { color: "#969696" }
        ]
      },{
        featureType: "landscape",
        stylers:[
          { visibility: "off"}
        ]
      },{
        featureType: "poi",
        stylers:[
          { visibility: "off" }
        ]
      }

  ];

  //define map Properties for each window size
  var mapProp = {
    center: new google.maps.LatLng(47.609215, 13.784317),
    zoom: zoomLevel,
    //disableDefaultUI:true, //turns off all google controls
    zoomControl: true,
    mapTypeControl: false,
    rotateControl: false,
    scaleControl: false,
    streetViewControl: false,
    fullscreenControl: true,
    mapTypeControlOptions: {
    mapTypeIds: [google.maps.MapTypeId.TERRAIN, "KlangOE"]
    }
  };

/*
  var mapProp1 = {
    center: new google.maps.LatLng(centerLat, centerLong),
    zoom: 8,
    disableDefaultUI:true, //turns off all google controls
    mapTypeControlOptions: {
    mapTypeIds: [google.maps.MapTypeId.TERRAIN, "KlangOE"]
    }
  };
  */

   var map = new google.maps.Map( document.getElementById("googleMap"), mapProp );

   zoomButton.addEventListener( 'click', function( ) {
     map.setZoom( zoomLevel );
     map.setCenter( mapProp.center );
   }, false )

   map.addListener( 'zoom_changed', function( ) {
     zoomButton.style.display = map.getZoom( ) > zoomLevel ? 'inline-block' : 'none';
   } )

  //set map properties according to window-size - deactivated for now....
  /*if (w > 770) {
    var map = new google.maps.Map( document.getElementById("googleMap"), mapProp );
  }else if (w == 582 ) {
    var map = new google.maps.Map( document.getElementById("googleMap"), mapProp1 );
  };
  */

  //load state borders
  map.data.loadGeoJson("oesterreich_1.json");

  //style state borders
  var layerStyle = {
    fillOpacity: 0,
    strokeWeight: 2,
    strokeColor: "#b70e0b"
  }

  //add state borders to map
  map.data.setStyle(layerStyle);

  var styledMapOptions = {name: "KlangOE"};

  var klangOEType = new google.maps.StyledMapType(
    klangOEStyles, styledMapOptions);

  map.mapTypes.set("KlangOE", klangOEType);
    map.setMapTypeId("KlangOE");


  //array to store sorted data
  var stations = [];
  //console.log(stations)

  //load data and sort it
  d3.tsv("Haeuser.tsv", function(error, data){
    var stationNames = d3.keys(data[0]).filter( function(key){ return key; } );

    data.forEach(function(d){
      d.stationValues = stationNames.map( function(name){ return d[name]; })
      stations.push(d.stationValues);
    });//closing data.forEach

    //add markers to map
    setMarkers(map, stations);

  });//closing data load


  var author = document.getElementById('author');
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(author);

}//closing initialize()

//define infowindow size
var infowindow = new google.maps.InfoWindow({
  maxWidth: 390,
  maxHeight: 350,
});

//function to set infowindow content
function setMarkers(map, locations){
  icon = {
    url: "pin_oe1_logo_rot_40_NEU.png",
  };

  var markers = [];

  locations.forEach(function( station ){
    var myLatLng = new google.maps.LatLng(station[6], station[7]);
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: icon,
      title: String(station[0]),
    });
    markers.push(marker);


    google.maps.event.addListener( marker, "mouseover", function(){
      var iconurl = marker.getIcon().url;
      var ll = marker.getPosition()
      if( iconurl != "pin_oe1_logo_grau_28_NEU.png"){
        icon2 = {url: "pin_oe1_logo_grau_40_NEU.png",};
         marker.setIcon(icon2);
      }
      marker.setPosition(ll);
    })

    google.maps.event.addListener( marker, "mouseout", function(){
      var iconurl = marker.getIcon().url;
      var ll = marker.getPosition()
      if( iconurl != "pin_oe1_logo_grau_28_NEU.png"){
        icon2 = {url: "pin_oe1_logo_rot_40_NEU.png"};
        marker.setIcon(icon2);
        marker.setPosition(ll);
      }
    })

    icon2 = {
          url: "pin_oe1_logo_grau_28_NEU.png"
        };

    //ToDo: seit dem timeout ganz unten funktioniert das "clicked" als kleine graue raute nich mehr (und zwar ab der Bewegung)
    google.maps.event.addListener(marker, "click", function(){
        var ll = marker.getPosition()
        marker.setIcon(icon2);
        marker.setPosition(ll);


      if( station[11] !="" ){
          //console.log(station[11])
          var p = "<image src ='Bilder/" + station[11] +"'></img>";
        }else{
          var p = "";
        }

         var a;
        if( station[12] !="" ){
          a = "<audio id='audiofile' controls preload ='audio' autobuffer><source src='" + station[12] + "'></audio>"
        }else{
          a = "<p>Audio demnächst.</p>"
        }
      
        
        var t = "<h1>"+station[0]+ " - " + station[1] + "</h1>"
        var o = "<p>"+station[4]+"</p>" //+station[2]+"</br>Architektur: "

        //var c = "<p>Link zur Sendung in Kürze.</p>";
        if( station[8] !="" ){
          //console.log(station[8])
          c = "<p><a href='http://oe1.orf.at/artikel/" + station[8] + "' target='_blank'>Mehr Infos</a></p>"
        }else{
          c = "<p>Link zur Sendung in Kürze.</p>";
        }
       
        var content =  '<div class="iw-content">' + t + p + o + a + c +'</div>';
        infowindow.setContent(content);
        infowindow.open(map,marker);
        setTimeout(function(){ 
                map.panBy(0, -50);
            }, 500);
    });
  });
  //var markerCluster = new MarkerClusterer(map, markers);
 var markerCluster = new MarkerClusterer(map, markers);

}//closing setMarkers()

//google.maps.event.addDomListener(window, 'load', initialize);
initialize( )
