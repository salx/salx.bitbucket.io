//app.js
// Vorlage: https://bl.ocks.org/mbostock/3709000

/*
ToDo

Nice to have:

Brush? siehe hier: https://bl.ocks.org/mbostock/1341021

Zoom: - Button, der die Achsen auf 0-100 setzt
Abstände der Achsen und Achsenbeschriftungen

Bezirke - Achse weiter nach rechts, oder ganz weg lassen

map vienna; Beispiel: So in der Art wie hier: https://vis.strategieanalysen.at/graz_bezirke/ https://www.ucas.com/corporate/data-and-analysis/ucas-undergraduate-releases/he-entry-rates
evtl: oben auf Achsen klicken und dann entsprechend dieser Achse sortieren.... ein Beispiel, wie das geht ist hier: http://bl.ocks.org/syntagmatic/3150059
ein Beispiel für sortierbare bar-Charts: https://bl.ocks.org/mbostock/3719724
hier ein Beispiel mit einer "brush" Funktion https://bl.ocks.org/mbostock/1341021 oder auch hier: https://bl.ocks.org/mbostock/1558011

*/

var margin = {top: 50, right: 102, bottom: 20, left: 155},
    width = 817 - margin.left - margin.right,
    height = 459 - margin.top - margin.bottom;

//anlegen der dimensions für die drei Achsen
var dimensions = [
	{
		name: "bezirke",
		scale: d3.scale.ordinal().rangePoints([0, height]),
		type: String
	},
	{
		name: "arbeitslosendichte",
		scale: d3.scale.linear().range([height, 0]),
		type: Number
	},
	{
		name: "wahlbeteiligung",
		scale: d3.scale.linear().range([height, 0]),
		type: Number
	},
	{
		name: "wahlausschluss",
		scale: d3.scale.linear().range([height, 0]),
		type: Number
	}
];

var x = d3.scale.ordinal()
	.domain(dimensions.map(function(d){ return d.name; }))
	.rangePoints([0, width]);

//x = d3.scale.ordinal().rangePoints([0, width], 1),

var y = {}; //brauche ich für die brush

var line = d3.svg.line()
	.defined(function(d) { return !isNaN(d[1]); });

var yAxis = d3.svg.axis()
	.orient("left");

var svg = d3.select(".container").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//svg.call(tip);

var dimension = svg.selectAll(".dimension")
	.data(dimensions)
	.enter().append("g")
	.attr("class", "dimension")
	.attr("transform", function(d){ return "translate(" + x(d.name) + ")"; });


//das Liniendiagramm Zeichnen
d3.tsv("DATEN.tsv", function(error, data){
	if (error) throw error;

	/* alter code
	dimensions.forEach(function(dimension){
		dimension.scale.domain(dimension.type == Number
			? d3.extent(data, function(d) { return +d[dimension.name]; })//
        	: data.map(function(d) { return d[dimension.name]; }).sort());
	});
	*/

	dimensions.forEach(function(dimension){
    var arg;
    if(dimension.type == Number){
      if(dimension.name== "arbeitslosendichte"){
        arg = d3.extent([0,16]);
      }if(dimension.name == "wahlbeteiligung"){
        //arg = d3.extent([64,86])
        arg = d3.extent([66,86]);
      }if(dimension.name == "wahlausschluss"){
        arg = d3.extent([0,45])
      }
    } if(dimension.type == String){
      arg = data.map(function(d) { return d[dimension.name]; })
    }
		dimension.scale.domain( arg )
	});

	//ToDo: das ist der Code, bei dem in der brush-Version ein Array aus y-Werten angelegt wird.
	// Extract the list of dimensions and create a scale for each.
  /*
  x.domain(dimensions = d3.keys(data[0]).filter(function(d) {
    return d != "name" && (y[d] = d3.scale.linear()//hier wird die erste Spalte ausgeblendet. Evt. Später mal...
        .domain(d3.extent(data, function(p) { return +p[d]; }))
        .range([height, 0]));
  }));
  */

	svg.append("g")
		.attr("class", "background")
		.selectAll("path")
		.data(data)
		.enter().append("path")
		.attr("d", draw);

	svg.append("g")
		.attr("class", "foreground")
		.selectAll("path")
		.data(data)
		.enter().append("path")
		.attr("d", draw);

	dimension.append("g")
		.attr("class", "axis")
		.each(function(d) { d3.select(this).call(yAxis.scale(d.scale)); })
    	.append("text") //wenn ich das lösche, sind sämtliche Beschriftungen weg
      	.attr("class", "title")
      	.attr("text-anchor", "middle")
      	.attr("y", -17)
      	//.text(function(d) { return d.name; });
      	.text(function(d){
      		var name;
      		if(d.name == "wahlausschluss"){
      			name = "nicht wahlberechtigt"
      		}else{
      			name = d.name
      		}
      		return name;
      	});

     
//append label that says "%", except for the first column (the districts)
     dimension.append("g")
     	.attr("class", "axis")
		.append("text")
		.attr("class", "title")
		.attr("text-anchor", "right")
		.attr("y", 10)
		.attr("x", 10)
		.text (function(d){
	     	var text;
	     	if(d.name == "arbeitslosendichte" || d.name == "wahlbeteiligung" || d.name == "wahlausschluss"){
		     	text = "%"
	     	} if (d.name == "bezirke"){
		     	text = ""
	     	}
	     	return text;
     	})

	//ToDo Add Brush-Event für Achsen
	/*
     dimension.append("g")
      .attr("class", "brush")
      .each(function(d) { (d.scale); d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brush", brush)); })
    .selectAll("rect")
      .attr("x", -8)
      .attr("width", 16);
     */
   

  // Rebind the axis data to simplify mouseover. - dachte erst, ich brauche das nicht, aber in Wirklichkeit für Labels-Interaktion
  svg.select(".axis").selectAll("text:not(.title)")
      .attr("class", "label")
      .data(data, function(d) { return d.bezirke || d;  });

  var projection = svg.selectAll(".axis text,.background path,.foreground path")
      .on("mouseover", mouseover)
      .on("mouseout", mouseout)

  function mouseover(d) {
 	var test = d.Kennzahl;
    if(test !== undefined){
    	svg.classed("active", true);
	    projection.classed("inactive", function(p) { return p !== d; }); //p liefert die komplette Datenliste
	    projection.filter(function(p) { return p === d; }).each(moveToFront);	
    }	
    //tip.show(d)
    //Zeige Info in Infobox, wenn mouse über eine Linie
    if(d.bezirke){
    	d3.selectAll(".d3-tip")
    	.html(+ d.Bezirk + "., " + d.bezirke + "</br>Arbeitslosendichte: " + d.arbeitslosendichte + "% Wahlbeteiligung: " + d.wahlbeteiligung + "% </br>Nicht Wahlber.: " 
    		+ d.wahlausschluss + "%")
    }

  }
    

  function mouseout(d) {
    svg.classed("active", false);
    projection.classed("inactive", false);
    //tip.hide(d);
    d3.selectAll(".d3-tip")
    	.html("Für Detailinformationen bitte mit der Maus einen Bezirk auswählen");

  }

  function moveToFront() {
    this.parentNode.appendChild(this);
  }

});

// ToDo: Handles a brush event, toggling the display of foreground lines.
function brush() {
  var actives = dimensions.filter(function(p) { return !y[p].brush.empty(); }),
      extents = actives.map(function(p) { return y[p].brush.extent(); });
  foreground.style("display", function(d) {
    return actives.every(function(p, i) {
      return extents[i][0] <= d[p] && d[p] <= extents[i][1];
    }) ? null : "none";
  });
}


function draw(d) {
	return line(dimensions.map(function(dimension) {
    return [x(dimension.name), dimension.scale(d[dimension.name])];
  }));
}
